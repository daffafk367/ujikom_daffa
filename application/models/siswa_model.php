<?php



class siswa_model extends CI_Model
{
    public function create()
    {
        if ($this->input->post('image') == "") {
            $image = "default.png";
        } else {
            $image = $this->input->post('image');
        }
        $data = array(

            'nisn' => $this->input->post('nisn'),
            'nis' => $this->input->post('nis'),
            'nama ' => $this->input->post('nama'),
            'id_kelas' => $this->input->post('id_kelas'),
            'alamat' => $this->input->post('alamat'),
            'no_telp' => $this->input->post('no_telp'),
            'id_spp' => $this->input->post('id_spp'),
            'username' => $this->input->post('username'),
            'password'  => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'role_id' => $this->input->post('role_id'),
            'date_created' => date("Y-m-d"),
            'is_active' => $this->input->post('is_active'),
            'image' => $image



        );
        $this->db->insert('siswa', $data);
    }

    public function read()
    {
        $query = $this->db->get('siswa');
        return $query;
    }

    public function read_by_id($nisn)
    {
        $query = $this->db->get_where('siswa', array('nisn' => $nisn));
        return $query;
    }


    public function update()
    {
        $data = array(

            'nisn' => $this->input->post('nisn'),
            'nis' => $this->input->post('nis'),
            'nama' => $this->input->post('nama'),
            'id_kelas' => $this->input->post('id_kelas'),
            'alamat' => $this->input->post('alamat'),
            'no_telp' => $this->input->post('no_telp'),
            'id_spp' => $this->input->post('id_spp'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'role_id' => $this->input->post('role_id'),
            'date_created' => date("Y-m-d"),
            'is_active' => $this->input->post('is_active'),
            'image' => $this->input->post('image')

        );
        $this->db->update('siswa', $data, array('nisn' => $this->input->post('nisn')));
    }

    public function delete($nisn)
    {
        $this->db->delete('siswa', array('nisn' => $nisn));
    }
}
