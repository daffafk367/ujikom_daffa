<?php



class petugas_model extends CI_Model
{
    public function create()
    {
        $data = array(
            'id_petugas' => $this->input->post('id_petugas'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'nama_petugas' => $this->input->post('nama_petugas'),
            'label' => $this->input->post('label')
            
            
        );
        $this->db->insert('petugas',$data);

    }

    public function read()
    {
        $query = $this->db->get('petugas');
        return $query;
    }
    public function read_by_id($id_petugas)
    {
        $query = $this->db->get_where('petugas',array('id_petugas' => $id_petugas));
        return $query;
    }

    public function update()
    {
        $data = array(

            'Id_petugas' => $this->input->post('id_petugas'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'nama_petugas' => $this->input->post('nama_petugas'),
            'label' => $this->input->post('label')
            
            
        );
        $this->db->update('petugas',$data, array ('id_petugas'=> $this->input->post('id_petugas')));
    }

    public function delete($id_petugas)
    {
        $this->db->delete('petugas',array('id_petugas'=> $id_petugas));
    }
        
}