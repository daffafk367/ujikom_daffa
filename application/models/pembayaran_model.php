<?php



class pembayaran_model extends CI_Model
{
    public function create()
    {
        $data = array(


            'nisn'          => $this->input->post('nisn'),
            'id_pembayaran' => $this->input->post('id_pembayaran'),
            'id_petugas'    => $this->input->post('id_petugas'),
            'bulan_dibayar' => $this->input->post('bulan_dibayar'),
            'tahun_dibayar' => $this->input->post('tahun_dibayar'),
            'tgl_bayar'     => date("Y-m-d"),
            'id_spp'        => $this->input->post('id_spp'),
            'jumlah_bayar'  => $this->input->post('jumlah_bayar')


        );
        $this->db->insert('pembayaran', $data);
    }

    public function read()
    {
        $query = $this->db->get('pembayaran');
        return $query;
    }
    public function read_by_id($id_pembayaran)
    {
        $query = $this->db->get_where('pembayaran', array('id_pembayaran' => $id_pembayaran));
        return $query;
    }

    public function read_by_nisn($nisn)
    {
        $query = $this->db->get_where('pembayaran', array('nisn' => $nisn));
        return $query;
    }


    public function update()
    {
        $data = array(

            'nisn'          => $this->input->post('nisn'),
            'id_petugas'    => $this->input->post('id_petugas'),
            'bulan_dibayar' => $this->input->post('bulan_dibayar'),
            'tahun_dibayar' => $this->input->post('tahun_dibayar'),
            'tgl_bayar'     => date("Y-m-d"),
            'id_spp'        => $this->input->post('id_spp'),
            'jumlah_bayar'  => $this->input->post('jumlah_bayar')


        );
        $this->db->update('pembayaran', $data, array('id_pembayaran' => $this->input->post('id_pembayaran')));
    }

    public function delete($id_pembayaran)
    {
        $this->db->delete('pembayaran', array('id_pembayaran' => $id_pembayaran));
    }
}
