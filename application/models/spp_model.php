<?php



class spp_model extends CI_Model
{
    public function create()
    {
        $data = array(
            'tahun' => $this->input->post('tahun'),
            'nominal' => $this->input->post('nominal')
            
        );
        $this->db->insert('spp',$data);

    }

    public function read()
    {
        $query = $this->db->get('spp');
        return $query;
    }

    public function read_by_id($id_spp)
    {
        $query = $this->db->get_where('spp',array('id_spp' => $id_spp));
        return $query;
    }

    public function update()
    {
        $data = array(
            'tahun' => $this->input->post('tahun'),
            'nominal' => $this->input->post('nominal')
            
        );
        $this->db->update('spp',$data, array ('id_spp'=> $this->input->post('id_spp')));
    }

    public function delete($id_spp)
    {
        $this->db->delete('spp',array('id_spp'=> $id_spp));
    }
        
}