<?php
defined('BASEPATH') or exit('No direct script access allowed');


class pembayaran extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembayaran_model');
    }
    public function index()
    {
        $data['query'] = $this->pembayaran_model->read();

        $data['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array();
        //var_dump($data['siswa']);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pembayaran/index', $data);
        $this->load->view('templates/footer');
    }

    public function read()
    {
        $data['query'] = $this->pembayaran_model->read();

        $data['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array();
        //var_dump($data['siswa']);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pembayaransaya/index', $data);
        $this->load->view('templates/footer');
    }



    public function edit($id_pembayaran)
    {
        $data['query'] = $this->pembayaran_model->read_by_id($id_pembayaran);

        $data['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array();
        //var_dump($data['siswa']);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pembayaran/edit', $data);
        $this->load->view('templates/footer');
    }
    public function update()
    {
        $id_pembayaran = $this->input->post('id_pembayaran');
        $nisn = $this->input->post('nisn');
        $id_spp = $this->input->post('id_spp');
        $id_pembayaran = $this->input->post('id_petugas');
        $bulan_dibayar = $this->input->post('bulan_dibayar');
        $tahun_dibayar = $this->input->post('tahun_dibayar');
        $jumlah_bayar = $this->input->post('jumlah_bayar');
        $tgl_bayar = $this->input->post('tgl_bayar');


        $data['query'] = $this->pembayaran_model->update();

        redirect('pembayaran');
    }

    public function add()
    {



        $data['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array();
        //var_dump($data['siswa']);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pembayaran/add');
        $this->load->view('templates/footer');
    }

    public function insert()
    {
        $nisn = $this->input->post('nisn');
        $id_pembayaran = $this->input->post('id_pembayaran');
        $id_petugas = $this->input->post('id_petugas');
        $tgl_bayar = $this->input->post('tgl_bayar');
        $bulan_bayar = $this->input->post('bulan_bayar');
        $tahun_bayar = $this->input->post('tahun_bayar');
        $id_spp = $this->input->post('id_spp');
        $jumlah_bayar = $this->input->post('jumlah_bayar');


        echo $id_pembayaran . " - " . $id_spp;

        $data['query'] = $this->pembayaran_model->create();

        redirect('pembayaran');
    }


    public function delete($id_pembayaran)
    {
        $this->pembayaran_model->delete($id_pembayaran);

        redirect('pembayaran');
    }
}
