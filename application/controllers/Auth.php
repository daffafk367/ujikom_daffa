<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->library('form_validation');
            $data['title'] = "SMK BPI BANDUNG";
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            //validasinya success
            $this->_login();
        }
    }


    private function _login()
    {

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $siswa = $this->db->get_where('siswa', ['username' => $username])->row_array();

        if ($siswa) {

            if ($siswa['is_active'] == 1) {

                if (password_verify($password, $siswa['password'])) {

                    $data = [
                        'username' => $siswa['username'],
                        'role_id' => $siswa['role_id'],

                    ];
                    $this->session->set_userdata($data);
                    if ($siswa['role_id'] == 1) {
                        redirect('admin');
                    }
                    redirect('dashboard');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Anda Salah!</div>');

                    redirect('auth');
                }
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Akun Anda Tidak Active!</div>');

                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Akun Anda Belum Terdaftar!</div>');

            redirect('auth');
        }
    }







    public function registration()
    {

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');

        $this->form_validation->set_rules(
            'password1',
            'Password',
            'required|trim|min_length[6]',
            [
                'min_length' => 'Password Terlalu Pendek'
            ]
        );


        // var_dump($this->form_validation->run());

        if ($this->form_validation->run() == false) {
            $data['title'] = "SMK BPI BANDUNG";
            $this->load->library('form_validation');
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
        } else {
            $data = [
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'nisn' => htmlspecialchars($this->input->post('nisn', true)),
                'nis' => htmlspecialchars($this->input->post('nis', true)),
                'id_kelas' => htmlspecialchars($this->input->post('id_kelas', true)),
                'alamat' => htmlspecialchars($this->input->post('alamat', true)),
                'no_telp' => htmlspecialchars($this->input->post('no_telp', true)),
                'id_spp' => htmlspecialchars($this->input->post('id_spp', true)),
                'username' => htmlspecialchars($this->input->post('username', true)),
                'image' => 'default.jpg',
                'password'  => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id'   => 2,
                "is_active" => 1,
                'date_created' => time()
            ];
            $this->db->insert('siswa', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Selamat Anda Berhasil Membuat Akun. Silahkan Login!</div>');
            redirect('auth');
        }
    }
    public function Logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda Telah Logout Terima Kasih!</div>');
        redirect('auth');
    }
}
