<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class petugas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('petugas_model');
    }
    public function index()
    {
        $data['query'] = $this->petugas_model->read();
        

        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('petugas/index',$data);
        $this->load->view('templates/footer');
    }
    public function edit($id_petugas)
    {
        $data['query'] = $this->petugas_model->read_by_id($id_petugas);

        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('petugas/edit',$data);
        $this->load->view('templates/footer');
    }
    public function update()
    {
        $id_petugas = $this->input->post('id_petugas');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $nama_petugas = $this->input->post('nama_petugas');
        $label = $this->input->post('label');
        

       $data['query'] = $this->petugas_model->update();

       redirect('petugas');
    }

    public function add()
    {
       
        
        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('petugas/add');
        $this->load->view('templates/footer');
    }

    public function insert()
    {
       $id_petugas = $this->input->post('id_petugas');
       $username = $this->input->post('username');
       $password = $this->input->post('password');
       $nama_petugas = $this->input->post('nama_petugas');
       $lebel = $this->input->post('lebel');
       echo $id_petugas ." - " . $nama_petugas;

       $data['query'] = $this->petugas_model->create();

       redirect('petugas');
    }


    public function delete($id_petugas)
    {
        $this->petugas_model->delete($id_petugas);

        redirect('petugas');
    }




}