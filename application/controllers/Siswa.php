<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Siswa extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('siswa_model');
    }
    public function index()
    {
        $data['query'] = $this->siswa_model->read();


        $data['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array();
        //var_dump($data['siswa']);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('siswa/index', $data);
        $this->load->view('templates/footer');
    }
    public function edit($nisn)
    {
        $data['query'] = $this->siswa_model->read_by_id($nisn);

        $data['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array();
        //var_dump($data['siswa']);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('siswa/edit', $data);
        $this->load->view('templates/footer');
    }
    public function update()
    {
        $nisn = $this->input->post('nisn');
        $nis = $this->input->post('nis');
        $nama = $this->input->post('nama');
        $id_kelas = $this->input->post('id_kelas');
        $alamat = $this->input->post('alamat');
        $no_telp = $this->input->post('no_telp');
        $id_spp = $this->input->post('id_spp');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $role_id = $this->input->post('role_id');
        $date_created = $this->input->post('date_created');
        $is_active = $this->input->post('is_active');
        $image = $this->input->post('image');
        echo $nisn . " - " . $nama;

        $data['query'] = $this->siswa_model->update();

        redirect('siswa');
    }

    public function add()
    {



        $data['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array();
        //var_dump($data['siswa']);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('siswa/add');
        $this->load->view('templates/footer');
    }

    public function insert()
    {
        $nisn = $this->input->post('nisn');
        $nis = $this->input->post('nis');
        $nama = $this->input->post('nama');
        $id_kelas = $this->input->post('id_kelas');
        $alamat = $this->input->post('alamat');
        $no_telp = $this->input->post('no_telp');
        $id_spp = $this->input->post('id_spp');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $role_id = $this->input->post('role_id');
        $date_created = $this->input->post('date_created');
        $is_active = $this->input->post('is_active');

        if ($this->input->post('image') != "") {
            $image = "default.png";
        } else {
            $image = $this->input->post('image');
        }


        echo $nisn . " - " . $nama;

        $data['query'] = $this->siswa_model->create();

        redirect('siswa');
    }


    public function delete($nisn)
    {
        $this->siswa_model->delete($nisn);

        redirect('siswa');
    }
}
