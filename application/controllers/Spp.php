<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class spp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('spp_model');
    }
    public function index()
    {
        $data['query'] = $this->spp_model->read();
        
        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('spp/index',$data);
        $this->load->view('templates/footer');
    }

    public function edit($id_spp)
    {
        $data['query'] = $this->spp_model->read_by_id($id_spp);

        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('spp/edit',$data);
        $this->load->view('templates/footer');
    }
    
    public function update()
    {
        $tahun = $this->input->post('id_spp');
        $tahun = $this->input->post('tahun');
       $nominal = $this->input->post('nominal');
       
       echo $tahun ." - " . $nominal;

       $data['query'] = $this->spp_model->update();

       redirect('spp');
    }

    public function add()
    {
       
        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('spp/add',$data);
        $this->load->view('templates/footer');
    }

    public function insert()
    {
       $tahun = $this->input->post('tahun');
       $nominal = $this->input->post('nominal');
       echo $tahun ." - " . $nominal;

       $data['query'] = $this->spp_model->create();

       redirect('spp');
    }


    public function delete($id_spp)
    {
        $this->spp_model->delete($id_spp);

        redirect('spp');
    }




}