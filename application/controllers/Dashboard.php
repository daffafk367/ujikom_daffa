<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        
        $data['siswa'] = $this->db->get_where('siswa',array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);
        //$this->load->view('templates/auth_header');
        //$this->load->view('auth/dashboard',$data);
        //$this->load->view('templates/auth_footer');
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('auth/dashboard',$data);
        $this->load->view('templates/footer');
    }
}
