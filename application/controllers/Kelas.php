<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class kelas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('kelas_model');
    }
    public function index()
    {
        $data['query'] = $this->kelas_model->read();

      

        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('kelas/index',$data);
        $this->load->view('templates/footer');
        
        
    }
    public function edit($id_kelas)
    {
        $data['query'] = $this->kelas_model->read_by_id($id_kelas);

        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('kelas/edit',$data);
        $this->load->view('templates/footer');
    }
    public function update()
    {
        $id_kelas = $this->input->post('id_kelas');
        $nama_kelas = $this->input->post('nama_kelas');
        $kompetensi_keahlian = $this->input->post('kompetensi_keahlian');


       $data['query'] = $this->kelas_model->update();

       redirect('kelas');
    }

    public function add()
    {
       
       
        $data ['title'] = 'my profile';
        $data['siswa'] = $this->db->get_where('siswa', array('username' => $this->session->userdata('username')))->row_array(); 
        //var_dump($data['siswa']);

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('kelas/add',$data);
        $this->load->view('templates/footer');
    }

    public function insert()
    {
       $id_kelas = $this->input->post('id_kelas');
       $nama_kelas = $this->input->post('nama_kelas');
       $kompetensi_keahlian = $this->input->post('kompetensi_keahlian');
       echo $id_kelas ." - " . $nama_kelas;

       $data['query'] = $this->kelas_model->create();

       redirect('kelas');
    }


    public function delete($id_kelas)
    {
        $this->kelas_model->delete($id_kelas);

        redirect('kelas');
    }




}