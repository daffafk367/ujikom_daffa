<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-warning sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
        <div class="sidebar-brand-icon">
        <i class="fas fa-school"></i>

        </div>
        <div class="sidebar-brand-text mx-3">SMK BPI </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider ">



        <!--QUERY MENU-->

        <?php
        $role_id = $this->session->userdata('role_id');
        $queryMenu=         "SELECT `user_menu`.`id`,`menu` 
                              FROM  `user_menu` JOIN `user_access_menu` 
                                ON  `user_menu`.`id` = `user_access_menu`.`menu_id` 
                             WHERE  `user_access_menu`.`role_id` = $role_id 
                            ORDER BY `user_access_menu`.`menu_id` ASC " ;    

        $menu = $this->db->query($queryMenu)->result_array();
        
        ?>

        
            <?php foreach ($menu as $m ) : ?>
        <div class="sidebar-heading">
        <?= $m['menu']; ?>
        </div>

                <?php
                $menuId = $m['id'];
                $querySubMenu =     "SELECT *
                                        FROM `user_sub_menu` JOIN `user_menu`
                                          ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
                                        WHERE `user_sub_menu`.`menu_id` = $menuId
                                        AND `user_sub_menu`.`is_active` = 1

                                    ";
                $subMenu = $this->db->query($querySubMenu)->result_array();
                ?>


                <?php foreach ($subMenu as $sm) : ?>
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url($sm['url']); ?>">
                        <i class="<?= $sm['icon'];?>"></i>
                            <span><?= $sm['title']?></span></a>
                    </li>
                <?php endforeach; ?>
                

                    
                <?php endforeach; ?>
    <!-- Nav Item - Dashboard -->


    <!-- Divider -->


 

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-user"> Edit Profile</i>
            
    </li>
     


    <!-- Divider -->
    <hr class="sidebar-divider">

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
            <i class="fas fa-fw fa-power-off"></i>
                <span>Logout</span></a>
                <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" href="<?= base_url('auth/logout'); ?>">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </li>


    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    <!-- Sidebar Message -->
    
</ul>
<!-- End of Sidebar -->
