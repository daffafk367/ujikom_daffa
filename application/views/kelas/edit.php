<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Edit Kelas </h1>
    
    <?php $row = $query->result(); ?>
    <form action="<?= base_url('kelas/update/') ?>" method="post">
    <div class="col-sm-6 mb-3 mb-sm-0">
            <input type="hidden" class="form-control form-control-user" name="id_kelas" id="id_kelas" value="<?= $row[0]->id_kelas?>">
            
            <label for="nama_kelas">Nama Kelas</label>
            <input type="text"   class="form-control form-control-user" name="nama_kelas" id="nama_kelas" value="<?= $row[0]->nama_kelas?>">
            
            <label for="kompetensi_keahlian">Kompetensi Keahlian</label>
            <input type="text" class="form-control form-control-user" name="kompetensi_keahlian" id="kompetensi_keahlian"value="<?= $row[0]->kompetensi_keahlian?>">
            
        <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
            <input type="Submit" class="btn btn-warning btn-user btn-block" value="Sunting Kelas">
        </div>
    </div> 
        
    </form>
</body>
</html>