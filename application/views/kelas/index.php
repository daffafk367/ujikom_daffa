<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kelas</title>
    <link href="<?=base_url('assets/');?>css/sb-admin-2.min.css" rel="stylesheet">
</head>
<!-- <style>
    table,
    th,
    td{
        border: 1px solid black;
    }
</style> -->
<body>
<h1 style="text-align:center;">Data Kelas</h1>
    <div class="container">
        <div class="row align-items-start">
            <div class="col">
                <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
                <table class="table">
                        
                        <thead class="table-warning">
                            <tr>
                            
                            <th scope="col">Nama Kelas</th>
                            <th scope="col">Kompetensi Keahlian</th>
                            <th scope="col">Aksi</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php foreach ($query->result() as $row){ ?>
                            <tr>
                                
                                <td><?= $row->nama_kelas?></td>
                                <td><?= $row->kompetensi_keahlian?></td>
                                <td>
                                <a class="btn btn-warning btn-user btn-block" href="<?= base_url('kelas/edit/') . $row->id_kelas?>">Edit</a>
                                <a class="btn btn-warning btn-user btn-block" href="<?= base_url('kelas/delete/') . $row->id_kelas?>">Delete</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    
                    </table>
                </div>
            </div>
        </div>
    </div>
         <h6>
            <div class="col-sm-3 mb-3 mb-sm-0 mt-4">
                <a class="btn btn-warning btn-user btn-block" href="<?= base_url('kelas/add/')?>">Tambah Data Kelas </a>
            </div>
        </h6>

     <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script>

    
</body>
</html>