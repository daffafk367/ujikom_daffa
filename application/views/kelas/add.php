<body>
    <h1>Tambah Kelas Baru</h1>
    
    <form action= "<?= base_url('kelas/insert/') ?>" method="post">
    <div class="col-sm-6 mb-3 mb-sm-0">
           
            <input type="hidden" class="form-control form-control-user" name="id_kelas" id="id_kelas">
            <label for="nama_kelas">Nama Kelas</label>
            <input type="text" class="form-control form-control-user" name="nama_kelas" id="nama_kelas">
            <label for="kompetensi_keahlian">Kompetensi Keahlian</label>
            <input type="text" class="form-control form-control-user" name="kompetensi_keahlian" id="kompetensi_keahlian">
        <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
            <input type="Submit" class="btn btn-warning btn-user btn-block" value="Tambah Kelas">
        </div>
    </div>
    </form>
    
</body>