<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Siswa</title>
</head>

<body>
    <h1>Tambah siswa Baru</h1>

    <form action="<?= base_url('siswa/insert/') ?>" method="post">
        <div class="col-sm-6 mb-3 mb-sm-0">
            <label for="nisn">Nisn</label>
            <input type="text" class="form-control form-control-user" name="nisn" id="nisn">

            <label for="nis">Nis</label>
            <input type="text" class="form-control form-control-user" name="nis" id="nis">

            <label for="nama">nama</label>
            <input type="text" class="form-control form-control-user" name="nama" id="nama">


            <input type="hidden" class="form-control form-control-user" name="id_kelas" id="id_kelas">

            <label for="alamat">alamat</label>
            <input type="text" class="form-control form-control-user" name="alamat" id="alamat">

            <label for="no_telp">no telp</label>
            <input type="text" class="form-control form-control-user" name="no_telp" id="no_telp">


            <input type="hidden" class="form-control form-control-user" name="id_spp" id="id_spp">

            <label for="username">Username</label>
            <input type="text" class="form-control form-control-user" name="username" id="username">

            <label for="password">Password</label>
            <input type="password" class="form-control form-control-user" name="password" id="password">

            <label for="role_id">role_id</label>
            <input type="text" class="form-control form-control-user" name="role_id" id="role_id">

            <label for="is_active">is_active</label>
            <input type="text" class="form-control form-control-user" name="is_active" id="is_active">

            <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
                <label for="image">image</label>
                <input type="file" class="form-control form-control-user" name="image" id="image" accept="image/png, image/jpg, image/jpeg, image/gif">
            </div>
        </div>
        <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
            <input type="Submit" class="btn btn-warning btn-user btn-block" value="Tambah siswa">
        </div>
    </form>
</body>

</html>