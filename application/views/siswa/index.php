<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Siswa</title>
</head>
<!-- <style>
    table,
    th,
    td{
        border: 1px solid black;
    }
</style> -->
<body>
<h1 style="text-align:center;">Data Siswa</h1>
    <div class="table-responsive">
        <div class="container">
            <div class="row align-items-start">
                <div class="col">
                    <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
                    
                    <table class="table">
                        
                            <thead class="table-warning">
                                <tr>
                                    <th scope="col">Nisn</th>
                                    <th scope="col">Nis</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">No telepon</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Date Created</th>
                                    <th scope="col">Is_Active</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Aksi</th>
                                </tr>

                            </thead>
                            
                            <tbody>
                                <?php foreach ($query->result() as $row){ ?>
                                <tr>
                                    <td><?= $row->nisn?></td>
                                    <td><?= $row->nis?></td>
                                    <td><?= $row->nama?></td>
                                    <td><?= $row->alamat?></td>
                                    <td><?= $row->no_telp?></td>
                                    <td><?= $row->username?></td>
                                    <td><?= $row->role_id?></td>
                                    <td><?= $row->date_created?></td>
                                    <td><?= $row->is_active?></td>
                                    <td><?= $row->image?></td>
                                    <td>
                                    <a class="btn btn-warning btn-user btn-block" href="<?= base_url('siswa/edit/') . $row->nisn?>">Edit </a>
                                    <a class="btn btn-warning btn-user btn-block" href="<?= base_url('siswa/delete/') . $row->nisn?>">Delete </a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                    <h6>
                        <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
                            <a class="btn btn-warning btn-user btn-block"  href="<?= base_url('siswa/add/')?>">Tambah Data Siswa </a>
                        </div>
                     </h6>
    
</body>
</html>