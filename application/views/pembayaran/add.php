<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pembayaran</title>
</head>
<body>
    <h1>Tambah pembayaran Baru</h1>
    
    <form action="<?= base_url('pembayaran/insert/') ?>" method="post">
    <div class="col-sm-6 mb-3 mb-sm-0">

            <input type="hidden" class="form-control form-control-user "name="id_pembayaran" id="id_pembayaran" >
            <input type="hidden" class="form-control form-control-user "name="id_petugas" id="id_petugas" >
            
            <label for="nisn">Nisn</label>
            <input type="text" class="form-control form-control-user" name="nisn" id="nisn" >
            
            <label for="bulan_dibayar">Pilih Bulan Dibayar:</label>
            <select id="bulan_dibayar" name="bulan_dibayar">
            <option value="Januari">Januari</option>
            <option value="February">February</option>
            <option value="Maret">Maret</option>
            <option value="April">April</option>
            <option value="Mei">Mei</option>
            <option value="Juni">Juni</option>
            <option value="Juli">Juli</option>
            <option value="Agustus">Agustus</option>
            <option value="September">September</option>
            <option value="Oktober">Oktober</option>
            <option value="November">November</option>
            <option value="Desember">Desember</option>
            </select>

            <label for="tahun_dibayar">Pilih Tahun Dibayar:</label>
            <select id="tahun_dibayar" name="tahun_dibayar">
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023">2023</option>
            <option value="2024">2024</option>
            <option value="2025">2025</option>
            <option value="2026">2026</option>
            <option value="2027">2027</option>
            <option value="2028">2028</option>
            <option value="2029">2029</option>
            <option value="2030">2030</option>
            </select>

            <label for="jumlah_bayar">Jumlah Bayar:</label>
            <select id="jumlah_bayar" name="jumlah_bayar">
            <option value="450000">450000</option>
            <option value="650000">650000</option>
            <option value="850000">850000</option>
            </select>
        <div class="col-sm- mb-3 mb-sm-0 mt-4">
            <label for="id_spp">Id Spp</label>
            <input type="text" class="form-control form-control-user "name="id_spp" id="id_spp" >
            <label for="id_petugas">Id Petugas</label>
            <input type="text" class="form-control form-control-user "name="id_petugas" id="id_petugas" >

            
        </div>
    </div>   
    <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
        <input type="Submit" class="btn btn-warning btn-user btn-block" value="Tambah pembayaran">
    </div>
    </form>
</body>
</html>