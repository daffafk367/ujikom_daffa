                <!-- Begin Page Content -->
    <div class="container-fluid">

                    <!-- Page Heading -->

                    <h1 class="h3 mb-4 text-gray-800">Dashboard</h1>

                             <div class = "row">
                    
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card border-left-primary shadow h-100 py-2">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                <a class="nav-link" href="<?= base_url('spp/index'); ?>">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        SPP</div>
                                                    </div>
                                                <a>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card border-left-primary shadow h-100 py-2">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                <a class="nav-link" href="<?= base_url('siswa/index'); ?>">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        Siswa</div>
                                                    </div>
                                                <a>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class = "row">
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card border-left-primary shadow h-100 py-2">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                <a class="nav-link" href="<?= base_url('kelas/index'); ?>">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        Kelas</div>
                                                    </div>
                                                <a>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card border-left-primary shadow h-100 py-2">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                <a class="nav-link" href="<?= base_url('petugas/index'); ?>">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        Petugas</div>
                                                    </div>
                                                <a>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card border-left-primary shadow h-100 py-2">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                <a class="nav-link" href="<?= base_url('pembayaran/index'); ?>">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        Pembayaran</div>
                                                    </div>
                                                <a>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    







                </div>
            </div>
                <!-- /.container-fluid -->
    </div>
            <!-- End of Main Content -->

           