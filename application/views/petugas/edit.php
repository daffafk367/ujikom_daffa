<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Edit Petugas </h1>

    <?php $row = $query->result(); ?>
    <form action="<?= base_url('petugas/update/') ?>" method="post">
        <div class="col-sm-6 mb-3 mb-sm-0">

            <input type="hidden" class="form-control form-control-user" name="id_petugas" id="id_petugas" value="<?= $row[0]->id_petugas ?>">

            <label for="username">Username</label>
            <input type="text" class="form-control form-control-user" name="username" id="username" value="<?= $row[0]->username ?>">

            <label for="password">Password</label>
            <input type="text" class="form-control form-control-user" name="password" id="password" value="<?= $row[0]->password ?>">

            <label for="nama_petugas">Nama Petugas</label>
            <input type="text" class="form-control form-control-user" name="nama_petugas" id="nama_petugas" value="<?= $row[0]->nama_petugas ?>">

            <label for="labek">Label</label>
            <input type="text" class="form-control form-control-user" name="label" id="label" value="<?= $row[0]->label ?>">

            <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
                <input type="Submit" class="btn btn-warning btn-user btn-block" value="Sunting Petugas">
            </div>
        </div>

    </form>
</body>

</html>