<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Petugas</title>
</head>
<!-- <style>
    table,
    th,
    td{
        border: 1px solid black;
    }
</style> -->
<body>
<h1 style="text-align:center;"> Data Petugas</h1>
    <div class="container">
        <div class="row align-items-start">
            <div class="col">
                <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
                <table class="table">
                        
                        <thead class="table-warning">
                            <tr>
                            
                            <th scope="col">Username</th>
                            <th scope="col">Password</th>
                            <th scope="col">Nama petugas</th>
                            <th scope="col">Label</th>
                            <th scope="col">Aksi</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php foreach ($query->result() as $row){ ?>
                            <tr>
                                
                                <td><?= $row->username?></td>
                                <td><?= $row->password?></td>
                                <td><?= $row->nama_petugas?></td>
                                <td><?= $row->label?></td>
                                <td>
                                    <a class="btn btn-warning btn-user btn-block" href="<?= base_url('petugas/edit/') . $row->id_petugas?>">Edit</a>
                                    <a class="btn btn-warning btn-user btn-block" href="<?= base_url('petugas/delete/') . $row->id_petugas?>">Delete</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
        <h6>
            <a class ="btn btn-warning btn-user btn-block" href="<?= base_url('petugas/add/') ?>">Tambah Data Petugas </a>
        </h6>
    </div>
    
</body>
</html>