<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Edit SPP </h1>
    <?php $row = $query->result(); ?>
    <form action="<?= base_url('spp/update/') ?>" method="post">
    <div class="col-sm-6 mb-3 mb-sm-0">
            <input type="hidden" name="id_spp" id="id_spp" value="<?= $row[0]->id_spp?>">
            <label for="tahun"> Tahun :</label>
            <select id="tahun" name="tahun">
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023">2023</option>
            <option value="2024">2024</option>
            <option value="2025">2025</option>
            <option value="2026">2026</option>
            <option value="2027">2027</option>
            <option value="2028">2028</option>
            <option value="2029">2029</option>
            <option value="2030">2030</option>
            </select>

            <label for="nominal">Nominal:</label>
            <select id="nominal" name="nominal">
            <option value="450000">450000</option>
            <option value="650000">650000</option>
            <option value="850000">850000</option>

            </select>
        <div class="col-sm-6 mb-3 mb-sm-0 mt-4">
            <input type="Submit" class="btn btn-warning btn-user btn-block" value="Sunting SPP">
        </div>
    </div> 
        
    </form>
</body>
</html>