<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pembayaran</title>
</head>
<!-- <style>
    table,
    th,
    td{
        border: 1px solid black;
    }
</style> -->

<body>
    <h1 style="text-align:center;">Data pembayaran</h1>
    <div class="container">
        <div class="row align-items-start">
            <div class="col">
                <div class="col-sm-50 mb-3 mb-sm-0 mt-4">
                    <table>

                        <table class="table">

                            <thead class="table-warning">
                                <tr>

                                    <th scope="col">Nisn</th>
                                    <th scope="col">Tanggal Bayar</th>
                                    <th scope="col">Bulan Dibayar</th>
                                    <th scope="col">Tahun Dibayar</th>
                                    <th scope="col">Jumlah Bayar</th>
                                    <th scope="col">Aksi</th>
                                    </th>

                                </tr>

                            </thead>
                            <tbody>
                                <?php foreach ($query->result() as $row) { ?>
                                    <tr>

                                        <td><?= $row->nisn ?></td>
                                        <td><?= $row->tgl_bayar ?></td>
                                        <td><?= $row->bulan_dibayar ?></td>
                                        <td><?= $row->tahun_dibayar ?></td>
                                        <td><?= $row->jumlah_bayar ?></td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>


</body>

</html>